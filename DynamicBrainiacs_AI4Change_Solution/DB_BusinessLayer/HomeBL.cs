﻿using DB_DataLayer;
using DB_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_BusinessLayer
{
    public class HomeBL
    {
        public HomeDL _homeDL = new HomeDL();

        public List<ProductDetails> GetAllProductDetails()
        {
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                if (System.Web.HttpContext.Current.Session["AllProductDetails"] == null)
                {
                    lstProductDetails = _homeDL.GetAllProductDetails();
                    System.Web.HttpContext.Current.Session["AllProductDetails"] = lstProductDetails;
                }
                else
                {
                    lstProductDetails = (List<ProductDetails>)System.Web.HttpContext.Current.Session["AllProductDetails"];
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstProductDetails;
        }

        public int CountOccurenceOfValue(List<int> list, int valueToFind)
        {
            try
            {
                return ((from temp in list where temp.Equals(valueToFind) select temp).Count());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void PlaceOrder(List<ProductDetails> lstProductDetails)
        {
            try
            {
                _homeDL.PlaceOrder(lstProductDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProductComponentDetails> GetAllAllergiesDetails()
        {
            List<ProductComponentDetails> lstAllergiesDetails = new List<ProductComponentDetails>();
            try
            {
                if (System.Web.HttpContext.Current.Session["AllAllergiesDetails"] == null)
                {
                    lstAllergiesDetails = _homeDL.GetAllAllergiesDetails();
                    System.Web.HttpContext.Current.Session["AllAllergiesDetails"] = lstAllergiesDetails;
                }
                else
                {
                    lstAllergiesDetails = (List<ProductComponentDetails>)System.Web.HttpContext.Current.Session["AllAllergiesDetails"];
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstAllergiesDetails;
        }

        public UserDetails EnrollUser(UserDetails userDetails)
        {
            try
            {
                userDetails = _homeDL.EnrollUser(userDetails);
                System.Web.HttpContext.Current.Session["CurrentUserDetails"] = userDetails;
            }
            catch (Exception ex)
            {
                throw;
            }
            return userDetails;
        }

        public WelcomeModel GetWelcomeDetailsForUser(string faceId)
        {
            WelcomeModel model = new WelcomeModel();
            try
            {
                model = _homeDL.GetWelcomeDetailsForUser(faceId);
                System.Web.HttpContext.Current.Session["CurrentUserDetails"] = model.UserDetails;
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<ProductDetails> GetRecommendations(string menuIDs)
        {
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                lstProductDetails = _homeDL.GetRecommendations(menuIDs);
            }
            catch (Exception)
            {

                throw;
            }
            return lstProductDetails;
        }
    }
}
