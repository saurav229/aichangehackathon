﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace DB_Logger
{
    public static class Logger
    {
        #region Variables
        public static string logFileName;
        #endregion

        static Logger()
        {
            if (!string.IsNullOrEmpty(GetSetting("LogFileName")))
            {
                logFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, GetSetting("LogFileName") + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log");

                IsDirectoryPresent(StripDirectoryName(logFileName), true);
            }
        }

        /// <summary>
        /// Gets Values From The Config File.
        /// </summary>
        public static string GetSetting(string val)
        {
            try
            {
                return ConfigurationManager.AppSettings[val];
            }
            catch (Exception ex)
            {
                WriteToLogFile(ex);
                return "";
            }
        }

        /// <summary>
        /// Gets The Directory Path from the FilePath
        /// </summary>
        public static string StripDirectoryName(string path)
        {
            string direcoryPath = @"";
            int indexOfLastSlash = 0;

            try
            {
                indexOfLastSlash = path.LastIndexOf(@"\");
                direcoryPath = path.Substring(0, indexOfLastSlash);
                return direcoryPath;
            }
            catch (Exception ex)
            {
                WriteToLogFile(ex);
                return "";
            }
        }

        /// <summary>
        /// Gets Values From The Config File.
        /// </summary>
        public static bool IsDirectoryPresent(string directory, bool create)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    if (create == true)
                    {
                        Directory.CreateDirectory(directory);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                WriteToLogFile(ex);
                return false;
            }
        }

        /// <summary>
        /// Writes the message to the Log File
        /// </summary>
        public static void WriteMessageToLogFile(string message, string assemblyName)
        {
            try
            {
                if (IsDirectoryPresent(StripDirectoryName(logFileName), true))
                {
                    FileStream fs = null;
                    StreamWriter sw = null;
                    string fileName;
                    DateTime dt = DateTime.Now;

                    try
                    {
                        fileName = logFileName;

                        fs = new FileStream(fileName, FileMode.Append, FileAccess.Write);
                        sw = new StreamWriter(fs);
                        sw.WriteLine("\n\nINFO\t" + dt.Month.ToString() + "-" + dt.Day.ToString() + "-" + dt.Year.ToString() + " " + dt.TimeOfDay.Hours.ToString("00") + ":" + dt.TimeOfDay.Minutes.ToString("00") + ":" + dt.TimeOfDay.Seconds.ToString("00") + "\t" + Process.GetCurrentProcess().Id + "\t" + assemblyName + "\t" + message);
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                    }
                    catch (Exception ex)
                    {
                        WriteMessageToLogFile(ex.Message, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                    }
                    finally
                    {
                        if (sw != null)
                        {
                            sw.Close();
                        }

                        if (fs != null)
                        {
                            fs.Close();
                        }
                    }
                }
            }
            catch (IOException ioEx)
            {
                WriteToLogFile(ioEx);
            }
        }

        /// <summary>
        /// Writes the exception to the FileSystem Watcher Log File
        /// </summary>
        public static void WriteToLogFile(Exception ex)
        {
            try
            {
                if (IsDirectoryPresent(StripDirectoryName(logFileName), true))
                {
                    FileStream fs = null;
                    StreamWriter sw = null;
                    string fileName;
                    DateTime dt = DateTime.Now;

                    try
                    {
                        fileName = logFileName;

                        fs = new FileStream(fileName, FileMode.Append, FileAccess.Write);
                        sw = new StreamWriter(fs);
                        if (GetSetting("EnableLowLevelLogging").ToLower().Equals("true"))
                        {
                            sw.WriteLine("\n\nERROR\t" + dt.Month.ToString() + "-" + dt.Day.ToString() + "-" + dt.Year.ToString() + " " + dt.TimeOfDay.Hours.ToString("00") + ":" + dt.TimeOfDay.Minutes.ToString("00") + ":" + dt.TimeOfDay.Seconds.ToString("00") + "\t" + Process.GetCurrentProcess().Id + "\t" + ex.Source + "\t" + ex.Message + "\t" + ex.InnerException + "\t" + ex.TargetSite + "\t" + ex.StackTrace);
                        }
                        else
                        {
                            sw.WriteLine("\n\nERROR\t" + dt.Month.ToString() + "-" + dt.Day.ToString() + "-" + dt.Year.ToString() + " " + dt.TimeOfDay.Hours.ToString("00") + ":" + dt.TimeOfDay.Minutes.ToString("00") + ":" + dt.TimeOfDay.Seconds.ToString("00") + "\t" + Process.GetCurrentProcess().Id + "\t" + ex.Source + "\t" + ex.Message);
                        }
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                    }
                    catch (Exception ex1)
                    {
                        WriteToLogFile(ex1);
                    }
                    finally
                    {
                        if (sw != null)
                        {
                            sw.Close();
                        }

                        if (fs != null)
                        {
                            fs.Close();
                        }
                    }
                }
            }
            catch (IOException ioEx)
            {
                WriteToLogFile(ioEx);
            }
        }

        public static void WriteToEventLog(string strEventType, int intEventID, string source, string strMessage)
        {
            try
            {
                EventLogEntryType type = EventLogEntryType.Warning;
                if (strEventType.Contains(Logger.GetSetting("EntryTypeInformation")))
                {
                    type = EventLogEntryType.Information;
                }
                else if (strEventType.Contains(Logger.GetSetting("EntryTypeError")))
                {
                    type = EventLogEntryType.Error;
                }
                EventLog.WriteEntry(source, strMessage, type, intEventID);
            }
            catch (Exception ex)
            {
                WriteToLogFile(ex);
            }
        }

        /// <summary>
        /// Execute the command
        /// </summary>
        /// <param name="strCommand"></param>
        /// <returns></returns>
        private static int ExecuteCommand(string strCommand)
        {
            int intExitCode = -1;
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = "cmd.exe";
                myProcess.StartInfo.Arguments = "/c \"" + strCommand + "\"";
                myProcess.Start();
                myProcess.WaitForExit();
                intExitCode = myProcess.ExitCode;
            }
            catch (Exception)
            {
                throw;
            }
            return intExitCode;
        }
    }
}
