﻿var albumBucketName = 'dtimagerekon';
var bucketRegion = 'us-east-1';
var IdentityPoolId = 'us-east-1:295b27c3-009f-4953-aaf4-d8cbf7d5bb3b';

AWS.config.update({
    region: bucketRegion,
    credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IdentityPoolId
    })
});

var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: albumBucketName }
});


