﻿function viewAlbum(albumName) {


    var albumPhotosKey = encodeURIComponent(albumName) + '/';
    s3.listObjects({ Prefix: albumPhotosKey }, function (err, data) {
        if (err) {
            //return alert('There was an error viewing your album: ' + err.message);
        }
        // `this` references the AWS.Response instance that represents the response
        var href = this.request.httpRequest.endpoint.href;
        var bucketUrl = href + albumBucketName + '/';

        var photos = data.Contents.map(function (photo) {
            var photoKey = photo.Key;
            var photoUrl = bucketUrl + (photoKey);
            fileExtension = photoUrl.split('.').pop();
            if (fileExtension === "") {
                //alert(fileExtension);
            }



            return getHtml([
                '<span>',
                '<div>',
                '<img style="width:128px;height:128px;" src="' + photoUrl + '"/>',
                '</div>',
                '<div>',
                '<span onclick="deletePhoto(\'' + albumName + "','" + photoKey + '\')">',
                'X',
                '</span>',
                '<span>',
                photoKey.replace(albumPhotosKey, ''),
                '</span>',
                '</div>',
                '</span>',
            ]);
        });
        var message = photos.length ?
            '<p>Click on the X to delete the photo</p>' :
            '<p>You do not have any photos in this album. Please add photos.</p>';
        var htmlTemplate = [
            '<h2>',
            'Album: ' + albumName,
            '</h2>',
            message,
            '<div>',
            getHtml(photos),
            '</div>',
            '<input id="photoupload" type="file" accept="image/*">',
            '<button id="addphoto" onclick="addPhoto(\'' + albumName + '\')">',
            'Add Photo',
            '</button>',
            '<button onclick="listAlbums()">',
            'Back To Albums',
            '</button>',
        ]
        document.getElementById('app').innerHTML = getHtml(htmlTemplate);
    });
}

function getHtml(template) {
    return template.join('\n');
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function addUserPhoto(albumName) {
    var imagename = $('#txtFirstName').val().trim() + "_" + $('#txtLastName').val().trim();
    imagename = (imagename.replace(/\s+/g, '')) + guid() + ".png";

    var fileName = imagename;
    var control = document.getElementById("imgSnapshot");
    var file = dataURLtoFile(control.src, fileName);


    var albumPhotosKey = (albumName) + '/';

    var photoKey = albumPhotosKey + fileName;
    s3.upload({
        Key: photoKey,
        Body: file,
        ACL: 'public-read'
    }, function (err, data) {
        RetrieveFACEID(imagename);

    });
}


function RetrieveFACEID(userImageName) {
    var dynamodb = new AWS.DynamoDB();

    var albumName = 'AI_IMAGEREKON';
    var ImageToFind = userImageName;
    var albumPhotosKey = encodeURIComponent(albumName) + '/';


    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
        TableName: 'AI_RekonCollection',
        ProjectionExpression: "RekognitionId, FullName",
        FilterExpression: "FullName = :FullName",
        ExpressionAttributeValues: {
            ":FullName": "AI_IMAGEREKON/" + userImageName
        }
    };
    console.log("Query succeeded Image Name. :" + userImageName);
    docClient.scan(params, function (err, data) {
        if (err) {
            console.log("Query succeeded.");
            //alert("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            console.log("Query succeeded.");
            //data.Items.forEach(function (item) {
            //    //alert(" Data -" + item.RekognitionId + ": " + item.FullName);
            //});
            debugger;
            if (data.Items != null && data.Items != undefined && data.Count > 0) {
                faceId = data.Items[0].RekognitionId;
                if (faceId != '') {
                    console.log(" Retrieved Face Id :" + faceId);
                    $('#matchresults').text(faceId);
                    EnrollUser();
                }
            }
            else {
                RetrieveFACEID(userImageName);
            }

        }
    });
}



function GetUserInfo(userImageName) {
    var albumName = userImageName.split("\/")[0];
    var ImageToFind = userImageName.split("\/")[1];
    var albumPhotosKey = encodeURIComponent(albumName) + '/';
    var photoUrl;

    s3.listObjects({ Prefix: albumPhotosKey }, function (err, data) {
        if (err) {
            return;
            //return alert('There was an error viewing your album: ' + err.message);
        }
        // `this` references the AWS.Response instance that represents the response
        var href = this.request.httpRequest.endpoint.href;
        var bucketUrl = href + albumBucketName + '/';
        var photos = data.Contents.map(function (photo) {
            var photoKey = photo.Key;
            photoUrl = bucketUrl + photoKey;

            //fileExtension = photoUrl.split('.').pop();
            if (photoKey === userImageName) {
                //Navigate('/Home/Menu');
                //$("#imgUser").attr('src', photoUrl);
                if (photoUrl != undefined)
                    localStorage.setItem("PhotoUrl", photoUrl);
                //$('#imagesList').attr('src', photoUrl);

                return getHtml([
                    '<span>',
                    '<div>',
                    '<img style="width:128px;height:128px;" src="' + photoUrl + '"/>',
                    '</div>',
                    '<div>',
                    '<span>',
                    photoKey.replace(albumPhotosKey, ''),
                    '</span>',
                    '</div>',
                    '</span>'
                ]);

            }

        });
        //var htmlTemplate = [
        //    '<h2>',
        //    'Album: ' + albumName,
        //    '</h2>',
        //    '<div>',
        //    getHtml(photos),
        //    '</div>'
        //];
        $("#appUserInfo").html(getHtml(htmlTemplate));
    });
    return photoUrl;
}




