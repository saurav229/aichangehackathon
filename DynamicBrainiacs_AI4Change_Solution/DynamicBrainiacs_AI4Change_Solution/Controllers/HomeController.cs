﻿using Amazon;
using Amazon.Lambda;
using Amazon.Lambda.Model;
using DB_BusinessLayer;
using DB_Entities;
using DB_Logger;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DynamicBrainiacs_AI4Change_Solution.Controllers
{
    public class HomeController : Controller
    {
        public HomeBL _homeBL = new HomeBL();
        public ActionResult Home()
        {
            if (System.Web.HttpContext.Current.Session["Count"] == null)
            {
                System.Web.HttpContext.Current.Session["Count"] = 0;
            }
            return View();
        }

        public ActionResult Enroll()
        {
            EnrollModel model = new EnrollModel();
            try
            {
                if (System.Web.HttpContext.Current.Session["Count"] == null)
                {
                    System.Web.HttpContext.Current.Session["Count"] = 0;
                }
                model.AllergiesList = _homeBL.GetAllAllergiesDetails();
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return View("Enroll", model);
        }

        public ActionResult Menu()
        {
            MenuModel menuModel = new MenuModel();
            try
            {
                if (System.Web.HttpContext.Current.Session["Count"] == null)
                {
                    System.Web.HttpContext.Current.Session["Count"] = 0;
                }
                menuModel.ProductDetailsList = _homeBL.GetAllProductDetails();
                menuModel.Counter = menuModel.ProductDetailsList.Count / 5.00;
                menuModel.Counter = Math.Ceiling(menuModel.Counter);
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return View("Menu", menuModel);
        }

        public ActionResult AboutUs()
        {
            if (System.Web.HttpContext.Current.Session["Count"] == null)
            {
                System.Web.HttpContext.Current.Session["Count"] = 0;
            }
            return View();
        }

        public JsonResult AddToCart(int productID)
        {
            List<int> lstProductIds = new List<int>();
            try
            {
                if (System.Web.HttpContext.Current.Session["Count"] == null)
                {
                    System.Web.HttpContext.Current.Session["Count"] = 0;
                }
                lstProductIds = (List<int>)System.Web.HttpContext.Current.Session["lstProductIds"];
                if (lstProductIds == null)
                {
                    lstProductIds = new List<int>();
                }
                lstProductIds.Add(productID);
                System.Web.HttpContext.Current.Session["lstProductIds"] = lstProductIds;
                System.Web.HttpContext.Current.Session["Count"] = lstProductIds.Count;
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return Json(lstProductIds.Count, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckOut()
        {
            List<int> lstProductIds = new List<int>();
            List<ProductDetails> lstAllProductDetails = new List<ProductDetails>();
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                if (System.Web.HttpContext.Current.Session["Count"] == null)
                {
                    System.Web.HttpContext.Current.Session["Count"] = 0;
                }
                if (System.Web.HttpContext.Current.Session["lstProductIds"] == null)
                {
                    System.Web.HttpContext.Current.Session["lstProductIds"] = new List<int>();
                }
                lstAllProductDetails = _homeBL.GetAllProductDetails();
                lstProductIds = (List<int>)System.Web.HttpContext.Current.Session["lstProductIds"];
                List<int> lstUniqueIds = lstProductIds.Distinct().ToList();

                ProductDetails details = new ProductDetails();
                foreach (int productId in lstUniqueIds)
                {
                    details = new ProductDetails();
                    details = lstAllProductDetails.Find(x => x.ProductID.Equals(productId));
                    details.ProductQyt = _homeBL.CountOccurenceOfValue(lstProductIds, productId);
                    details.ProductTotal = details.ProductAmount * details.ProductQyt;
                    lstProductDetails.Add(details);
                }
                System.Web.HttpContext.Current.Session["lstProductDetailsToSave"] = lstProductDetails;
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return View("CheckOut", lstProductDetails);
        }

        public ActionResult PlaceOrder()
        {
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                System.Web.HttpContext.Current.Session["Count"] = 0;
                System.Web.HttpContext.Current.Session["lstProductIds"] = null;

                lstProductDetails = (List<ProductDetails>)System.Web.HttpContext.Current.Session["lstProductDetailsToSave"];
                _homeBL.PlaceOrder(lstProductDetails);
                System.Web.HttpContext.Current.Session["lstProductDetailsToSave"] = null;
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return RedirectToAction("Home");
        }

        public void EnrollUser(UserDetails userDetails)
        {
            try
            {
                _homeBL.EnrollUser(userDetails);
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
        }

        public ActionResult Welcome(string faceId)
        {
            WelcomeModel model = new WelcomeModel();
            try
            {
                //faceId = "b4abc1c5-b866-4730-9750-c21c01dbd876";
                model = _homeBL.GetWelcomeDetailsForUser(faceId);
                string menuIDs = GetRecommendationsMenuIDs(model.UserDetails.UserID);
                model.Recommendations = _homeBL.GetRecommendations(menuIDs);
            }
            catch (Exception ex)
            {
                Logger.WriteToLogFile(ex);
            }
            return View("Welcome", model);
        }

        private string GetRecommendationsMenuIDs(int userID)
        {
            string result = string.Empty;
            try
            {
                Dictionary<string, string> paylod = new Dictionary<string, string>();

                using (var client = new AmazonLambdaClient("AKIAJYMRM6MTDVGMAHNQ", "LpGWKrr6alaFwoUbDu36Zh16KwfL27wm3rtvrAba", RegionEndpoint.USEast1))
                {
                    paylod.Add("user_id", userID.ToString());
                    var request = new InvokeRequest
                    {
                        FunctionName = "AI_LAMBDARECOMMENDATION",
                        InvocationType = InvocationType.RequestResponse,
                        Payload = Newtonsoft.Json.JsonConvert.SerializeObject(paylod)
                    };

                    var response = client.Invoke(request);                    
                    using (var sr = new StreamReader(response.Payload))
                    {
                        result = sr.ReadToEnd();
                    }
                    result = result.Replace("\\\"", "");
                    JObject json = JObject.Parse(result);
                    result = json["body"].ToString();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }
    }
}