﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB_Entities;

namespace DB_DataLayer
{
    public class HomeDL
    {
        public void PlaceOrder(List<ProductDetails> lstProductDetails)
        {
            UserDetails userDetails = new UserDetails();
            try
            {
                userDetails = (UserDetails)System.Web.HttpContext.Current.Session["CurrentUserDetails"];
                double totalAmount = 0.0;
                DataTable dt = new DataTable();
                dt.Columns.Add("ProductID");
                dt.Columns.Add("ProductQyt");

                foreach (ProductDetails item in lstProductDetails)
                {
                    DataRow row = dt.NewRow();
                    row["ProductID"] = item.ProductID;
                    row["ProductQyt"] = item.ProductQyt;
                    totalAmount += (item.ProductAmount * item.ProductQyt);
                    dt.Rows.Add(row);
                }

                SqlParameter[] parameters = new SqlParameter[]
             {
                    new SqlParameter("@UserID", userDetails.UserID),
                    new SqlParameter("@TotalAmount", totalAmount),
                    new SqlParameter("@ProductDetailsType", SqlDbType.Structured)
                {
                    TypeName = "dbo.ProductDetailsType",
                    Value = dt
                }
            };
                if (SqlDBHelper.ExecuteNonQuery("PlaceOrder", CommandType.StoredProcedure, parameters))
                {
                    System.Web.HttpContext.Current.Session["CurrentUserDetails"] = new UserDetails();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProductDetails> GetAllProductDetails()
        {
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                using (DataTable dt = SqlDBHelper.ExecuteSelectCommand("GetAllProductDetails", CommandType.StoredProcedure))
                {
                    lstProductDetails = dt.AsEnumerable()
                    .Select(row => new ProductDetails
                    {
                        ProductID = row.Field<int>(0),
                        ProductName = row.Field<string>(1).Trim(),
                        ProductDescription = row.Field<string>(2).Trim(),
                        ProductImageURL = row.Field<string>(3).Trim(),
                        ProductAmount = row.Field<double>(4),
                        ProductCal = row.Field<int>(5)
                    }).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstProductDetails;
        }

        public List<UserDetails> GetAllUserDetails()
        {
            List<UserDetails> lstUserDetails = new List<UserDetails>();
            try
            {
                using (DataTable dt = SqlDBHelper.ExecuteSelectCommand("GetAllUserDetails", CommandType.StoredProcedure))
                {
                    lstUserDetails = dt.AsEnumerable()
                    .Select(row => new UserDetails
                    {
                        UserID = row.Field<int>(0),
                        FirstName = row.Field<string>(1).Trim(),
                        LastName = row.Field<string>(2).Trim(),
                        MobileNumber = row.Field<string>(3).Trim(),
                        Email = row.Field<string>(4)
                    }).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstUserDetails;
        }

        public List<ProductDetails> GetRecommendations(string menuIDs)
        {
            List<ProductDetails> lstProductDetails = new List<ProductDetails>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[]
             {
                    new SqlParameter("@MenuIDs", menuIDs)
             };
                using (DataTable dt = SqlDBHelper.ExecuteParamerizedSelectCommand("GetRecommendations", CommandType.StoredProcedure, parameters))
                {
                    lstProductDetails = dt.AsEnumerable()
                    .Select(row => new ProductDetails
                    {
                        ProductID = row.Field<int>(0),
                        ProductName = row.Field<string>(1).Trim(),
                        ProductDescription = row.Field<string>(2).Trim(),
                        ProductImageURL = row.Field<string>(3).Trim(),
                        ProductAmount = row.Field<double>(4),
                        ProductCal = row.Field<int>(5)
                    }).Take(2).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstProductDetails;
        }

        public WelcomeModel GetWelcomeDetailsForUser(string faceId)
        {
            WelcomeModel model = new WelcomeModel();
            List<UserDetails> lstUserDetails = new List<UserDetails>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[]
             {
                    new SqlParameter("@FaceId", faceId)
             };
                using (DataTable dt = SqlDBHelper.ExecuteParamerizedSelectCommand("GetUserDetailsByFaceID", CommandType.StoredProcedure, parameters))
                {
                    lstUserDetails = dt.AsEnumerable()
                    .Select(row => new UserDetails
                    {
                        UserID = row.Field<int>(0),
                        FirstName = row.Field<string>(1).Trim(),
                        LastName = row.Field<string>(2).Trim(),
                        MobileNumber = row.Field<string>(3).Trim(),
                        Email = row.Field<string>(4),
                        Allergies = string.IsNullOrEmpty(row.Field<string>(5)) ? null : row.Field<string>(5).Split(',')
                    }).ToList();
                }
                model.UserDetails = lstUserDetails[0];

                parameters = new SqlParameter[]
             {
                    new SqlParameter("@UserId", model.UserDetails.UserID)
             };
                using (DataTable dt = SqlDBHelper.ExecuteParamerizedSelectCommand("GetOrderHistoryByUserId", CommandType.StoredProcedure, parameters))
                {
                    model.OrderHistory = dt.AsEnumerable()
                    .Select(row => new ProductDetails
                    {
                        ProductID = row.Field<int>(0),
                        ProductName = row.Field<string>(1).Trim(),
                        ProductDescription = row.Field<string>(2).Trim(),
                        ProductImageURL = row.Field<string>(3).Trim(),
                        ProductAmount = row.Field<double>(4),
                        ProductCal = row.Field<int>(5)
                    }).Take(2).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<UserDetails> GetUserDetailsByUserID(int userID)
        {
            UserDetails userDetails = new UserDetails();
            List<UserDetails> lstUserDetails = new List<UserDetails>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[]
             {
                    new SqlParameter("@UserId", userID)
             };
                using (DataTable dt = SqlDBHelper.ExecuteParamerizedSelectCommand("GetUserDetailsByUserID", CommandType.StoredProcedure, parameters))
                {
                    lstUserDetails = dt.AsEnumerable()
                    .Select(row => new UserDetails
                    {
                        UserID = row.Field<int>(0),
                        FirstName = row.Field<string>(1).Trim(),
                        LastName = row.Field<string>(2).Trim(),
                        MobileNumber = row.Field<string>(3).Trim(),
                        Email = row.Field<string>(4),
                        Allergies = string.IsNullOrEmpty(row.Field<string>(5)) ? null : row.Field<string>(5).Split(',')
                    }).ToList();
                }
                userDetails = lstUserDetails[0];
            }
            catch (Exception)
            {
                throw;
            }
            return lstUserDetails;
        }

        public UserDetails EnrollUser(UserDetails userDetails)
        {
            UserDetails tempUserDetails = new UserDetails();
            try
            {
                DataTable dtTableType = new DataTable();
                dtTableType.Columns.Add("ID");

                if (userDetails.Allergies != null)
                {
                    foreach (string item in userDetails.Allergies)
                    {
                        DataRow row = dtTableType.NewRow();
                        row["ID"] = Convert.ToInt32(item);
                        dtTableType.Rows.Add(row);
                    }
                }
                SqlParameter[] parameters = new SqlParameter[]
             {
                    new SqlParameter("@FirstName", userDetails.FirstName),
                    new SqlParameter("@LastName", userDetails.LastName),
                    new SqlParameter("@MobileNumber", userDetails.MobileNumber),
                    new SqlParameter("@Email", userDetails.Email),
                    new SqlParameter("@FaceId", userDetails.FaceId),
                    new SqlParameter("@Allergies", SqlDbType.Structured)
                {
                    TypeName = "dbo.AllergiesType",
                    Value = dtTableType
                }
             };
                using (DataTable dt = SqlDBHelper.ExecuteParamerizedSelectCommand("EnrollUserDetails", CommandType.StoredProcedure, parameters))
                { }

                using (DataTable dt = SqlDBHelper.ExecuteSelectCommand("GetLastestUserId", CommandType.StoredProcedure))
                {
                    tempUserDetails = dt.AsEnumerable()
                        .Select(row => new UserDetails
                        {
                            UserID = row.Field<int>(0)
                        }).ToList()[0];
                }
                userDetails.UserID = tempUserDetails.UserID;
            }
            catch (Exception ex)
            {
                throw;
            }
            return userDetails;
        }

        public List<ProductComponentDetails> GetAllAllergiesDetails()
        {
            List<ProductComponentDetails> lstAllergiesDetails = new List<ProductComponentDetails>();
            try
            {
                using (DataTable dt = SqlDBHelper.ExecuteSelectCommand("GetAllAllergies", CommandType.StoredProcedure))
                {
                    lstAllergiesDetails = dt.AsEnumerable()
                    .Select(row => new ProductComponentDetails
                    {
                        ProductComponentID = row.Field<int>(0),
                        ProductComponentName = row.Field<string>(1).Trim(),
                        ProductComponentDescription = row.Field<string>(2).Trim()
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstAllergiesDetails;
        }
    }
}
