﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Entities
{
    public class Model
    {
    }

    public class ProductDetails
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public double ProductAmount { get; set; }
        public int ProductCal { get; set; }
        public string ProductImageURL { get; set; }
        public int ProductQyt { get; set; }
        public double ProductTotal { get; set; }
    }

    public class ProductComponentDetails
    {
        public int ProductComponentID { get; set; }
        public string ProductComponentName { get; set; }
        public string ProductComponentDescription { get; set; }
    }

    public class UserDetails
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string FaceId { get; set; }
        public string[] Allergies { get; set; }
    }

    public class EnrollModel
    {
        public List<ProductComponentDetails> AllergiesList { get; set; }
    }

    public class MenuModel
    {
        public double Counter { get; set; }
        public List<ProductDetails> ProductDetailsList { get; set; }
    }

    public class WelcomeModel
    {
        public UserDetails UserDetails { get; set; }
        public List<ProductDetails> OrderHistory { get; set; }
        public List<ProductDetails> Recommendations { get; set; }
    }
}
